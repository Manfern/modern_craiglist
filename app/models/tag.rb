class Tag < ApplicationRecord
	belongs_to :section
	belongs_to :ad
	validates :tag_type, presence: true # название тэга присутствует

end