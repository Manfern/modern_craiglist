class Ad < ApplicationRecord
	belongs_to :user
	has_many :conversations
	has_many :tags

	validates :ad_text, presence: true #обязательное поле
	validates :title, presence: true #обязательное поле
	validates_associated :tags #проверить тэги при сохранении
end
