class User < ApplicationRecord
	has_many :ads #У пользователя может быть много объявлений
	has_many :profiles #У пользователя может быть много профилей(для истории изменений)
	validates :email, presence: true, uniqueness: true, confirmation: true #т.к. нельзя менять и является логином, то требуется 2 раза ввести для подтверждения. Также проверка на уникальность
	validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } # проверка email на соотвествие формату
	validates :password, presence: true, confirmation: true, length: { in: 4..20 } #требуется для входа
	validates :name, presence: true, length: { minimum: 3 } #обязательное поле
	validates :birth_date, presence: true #требуется для проверки возраста
	validate :age_check #проверка возраста для регистрации
	validate :phone, numericality: true #проверка телефона
  private

	  def age_check
	      if birth_date.present? && birth_date > 18.years.ago.to_d
	          errors.add(:birth_date, 'Вам еще нет 18ти лет')
	      end
	  end

end
