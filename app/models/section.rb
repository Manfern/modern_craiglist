class Section < ApplicationRecord
	has_many :tags
	validates :name, presence: true #есть название секции
end
