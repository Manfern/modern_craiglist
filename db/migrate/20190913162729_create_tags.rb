class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags do |t|
      t.string :tag_type
      t.integer :used_count

      t.timestamps
    end
  end
end
