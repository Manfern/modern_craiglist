class CreateConversations < ActiveRecord::Migration[5.2]
  def change
    create_table :conversations do |t|
      t.string :ad_author
      t.string :second_user

      t.timestamps
    end
  end
end
