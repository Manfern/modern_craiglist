class CreateAds < ActiveRecord::Migration[5.2]
  def change
    create_table :ads do |t|
      t.string :title
      t.text :ad_text
      t.string :location
      t.string :ad_status
      t.datetime :publish_date
      t.string :title_highlight
      t.datetime :promote_till
      t.integer :violations
      t.string :users_reported

      t.timestamps
    end
  end
end
