class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email
      t.boolean :email_verified
      t.string :password
      t.datetime :birth_date
      t.string :name
      t.string :phone
      t.boolean :phone_verified
      t.datetime :registrated
      t.string :user_status
      t.datetime :block_ended
      t.integer :point_balance
      t.string :location, :array => true #массив для положения страна и город, 2 строки

      t.timestamps
    end
  end
end
