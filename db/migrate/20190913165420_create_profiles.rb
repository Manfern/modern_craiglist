class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :date_birth
      t.string :name
      t.string :phone

      t.timestamps
    end
  end
end
